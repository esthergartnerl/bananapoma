#version 300 es
precision mediump float;

// Vertices attributes
layout(location = 0) in vec3 aVertexPosition;
layout(location = 1) in vec3 aVertexNormal;
layout(location = 2) in vec2 aVertexTexCoords;

uniform mat4 uMVPMatrix;
uniform mat4 uMVMatrix;
uniform mat4 uNormalMatrix;

// Material attributes
uniform vec3 uAmbient;
uniform vec3 uDiffuse;
uniform vec3 uSpecular;

out vec3 vPosition; // Vertex position in view space
out vec3 vNormal; // Normal in view space
out vec2 vTexCoords;

out vec3 vAmbient;
out vec3 vDiffuse;
out vec3 vSpecular;

void main(){
  vec4 position = vec4(aVertexPosition, 1.);
  vec4 normal = vec4(aVertexNormal, 0.);

  vPosition = vec3(uMVMatrix * position);
  vNormal = vec3(uNormalMatrix * normal);
  vTexCoords = aVertexTexCoords;

  // Sends material attributes to the fragment shader
  vAmbient = uAmbient;
  vDiffuse = uDiffuse;
  vSpecular = uSpecular;

  gl_Position = uMVPMatrix * position;
}
