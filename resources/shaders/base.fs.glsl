#version 300 es
precision mediump float;

// Sorties du shader
in vec3 vPosition; // Vertex position in view space
in vec3 vNormal;
in vec2 vTexCoords; // Normal in view space

// Material attributes
in vec3 vAmbient;
in vec3 vDiffuse;
in vec3 vSpecular;

struct DirectionalLight{
  vec3 direction;
  vec3 intensity;
};
#define NR_DIRECTIONAL_LIGHTS 4 // Must correspond to the number of lights in assets/lights.txt
uniform DirectionalLight directional_lights[NR_DIRECTIONAL_LIGHTS];

struct PointLight{
  vec3 position;
  vec3 intensity;
};
#define NR_POINT_LIGHTS 9 // Must correspond to the number of lights in assets/lights.txt
uniform PointLight point_lights[NR_POINT_LIGHTS];

out vec3 fFragColor;

uniform sampler2D texture_diffuse1;

vec3 calcDirectionalLight(float shininess, vec3 light_dir, vec3 light_intensity){
  // BlinnPhong model
  vec3 diffuse = vDiffuse + texture(texture_diffuse1, vTexCoords).xyz;
  vec3 wi = normalize(light_dir);
  vec3 w0 = normalize(-vPosition);
  vec3 N = vNormal;
  vec3 color = light_intensity * (diffuse * (max(0.0, dot(wi, N)) + vSpecular * pow(max(0.0, dot(reflect(-wi, N), w0)), shininess)));
  return color;
}

vec3 calcPointLight(float shininess, vec3 light_pos, vec3 light_intensity){
  // BlinnPhong model
  vec3 diffuse = vDiffuse + texture(texture_diffuse1, vTexCoords).xyz;
  float d = distance(light_pos, vPosition);
  vec3 Li = light_intensity / pow(d, 2.f);
  vec3 wi = normalize(light_pos - vPosition);
  vec3 w0 = normalize(-vPosition);
  vec3 N = vNormal;
  vec3 color = Li * (diffuse * (max(0.0, dot(wi, N)) + vSpecular * pow(max(0.0, dot(reflect(-wi, N), w0)), shininess)));
  return color;
}

void main() {
  vec3 color_output = vec3(0.); // Make sure to initialize it to avoid rendering issues (depends on the graphic card)

  for(int i = 0; i < NR_DIRECTIONAL_LIGHTS; i++)
    color_output += calcDirectionalLight(1.0f, directional_lights[i].direction, directional_lights[i].intensity);
  for(int i = 0; i < NR_POINT_LIGHTS; i++)
    color_output += calcPointLight(1.0f, point_lights[i].position, point_lights[i].intensity);

  fFragColor = color_output;
}
