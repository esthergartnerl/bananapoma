//#version 330 core
#version 300 es
precision mediump float;

in vec2 vTexCoords;

out vec4 fColor;

uniform sampler2D text; // text texture
uniform vec3 textColor;

void main()
{
    vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, vTexCoords).r);
    fColor = vec4(textColor, 1.0) * sampled;
}