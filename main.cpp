#include <iostream>
#include "src/Game.hpp"

Game Game::_singleton;
uint32_t Game::_screen_width = 1280;
uint32_t Game::_screen_height = 720;
unsigned int Game::_frame_rate = 60;

int main() {
    Game& game = Game::get();
    game.initialize();
    game.load();
    game.run();
    game.free();
    return 0;
}
