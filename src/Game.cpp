#include "Game.hpp"
#include <iostream>

Game::Game() noexcept:_running(true), _window_manager(_screen_width, _screen_height, "BANANAPOMA"),
_events_manager(),_graphics_engine(_screen_width, _screen_height), _game_engine(), _sound_engine() {}

void Game::initialize() {
    _window_manager.initializeSDLOpenGlContext();
    _game_engine.initialize();
    _sound_engine.initialize();
    _graphics_engine.initialize(_game_engine.getCookingPot(), _frame_rate);
    _events_manager.initialize(_running, _graphics_engine, _game_engine, _sound_engine, _window_manager);
}

void Game::load() {
    _game_engine.loadResources();
    _sound_engine.loadResources();
    _graphics_engine.loadResources();
}

void Game::run() {
#ifdef VERBOSE
    std::cout << "---------- Début boucle de jeu ----------" << std::endl;
#endif
    _sound_engine.playMusic();
    while (_running)
    {
        _events_manager.manageEvents();
        _graphics_engine.draw(_window_manager);
    }
#ifdef VERBOSE
    std::cout << "----------  Fin boucle de jeu  ----------" << std::endl;
#endif
}

void Game::free() {
    _game_engine.freeResources();
    _sound_engine.freeResources();
    _graphics_engine.freeResources();
    _window_manager.freeResources();
}
