#include "../include/Model.hpp"

/*
 * Code from : https://learnopengl.com
 * Modified
 */

void Model::draw(std::unordered_map<std::string, ShaderManager>& shaders)
{
    for(auto& mesh : _meshes)
        mesh.draw(shaders, _graphical_id);
}

void Model::drawOutline()
{
    for(auto& mesh : _meshes)
        mesh.drawOutline(_graphical_id);
}

void Model::initialize(const std::string& pathModel, const glm::vec3& position) {
    _path = pathModel;
    _collision_sphere = CollisionSphere(position, 0.5); // TODO: determine a radius in function of the model's geometry
}

void Model::load()
{
    Assimp::Importer import;
    const aiScene *scene = import.ReadFile(_path, aiProcess_Triangulate | aiProcess_FlipUVs);
    if(!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
    {
        std::cout << "ERROR::ASSIMP::" << import.GetErrorString() << std::endl;
        return;
    }
    processNode(scene->mRootNode, scene);
}


void Model::processNode(aiNode *node, const aiScene *scene)
{
    // for every meshes of the node
    for(unsigned int i = 0; i < node->mNumMeshes; i++)
    {
        aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
        _meshes.push_back(processMesh(mesh, scene));
    }
    // do the same on children
    for(unsigned int i = 0; i < node->mNumChildren; i++)
    {
        processNode(node->mChildren[i], scene);
    }
}

Mesh Model::processMesh(aiMesh *mesh, const aiScene *scene)
{
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<Texture> textures;
    for(unsigned int i = 0; i < mesh->mNumVertices; i++)
    {
        Vertex vertex;
        // Positions
        glm::vec3 vPos;
        vPos.x = mesh->mVertices[i].x;
        vPos.y = mesh->mVertices[i].y;
        vPos.z = mesh->mVertices[i].z;
        vertex._position = vPos;

        // Normals
        if(mesh->HasNormals())
        {
          glm::vec3 vNormal;
          vNormal.x = mesh->mNormals[i].x;
          vNormal.y = mesh->mNormals[i].y;
          vNormal.z = mesh->mNormals[i].z;
          vertex._normal = vNormal;
        }

        // Texture coordinates
        if(mesh->mTextureCoords[0]) // are there texture coordinates ?
        {
          glm::vec2 tex;
          tex.x = mesh->mTextureCoords[0][i].x;
          tex.y = mesh->mTextureCoords[0][i].y;
          vertex._tex_coords = tex;
        }
        else
        {
          vertex._tex_coords = glm::vec2(0.0f, 0.0f);
        }
        vertices.push_back(vertex);
    }

    // Indices
    for(unsigned int i = 0; i < mesh->mNumFaces; i++)
    {
    aiFace face = mesh->mFaces[i];
    for(unsigned int j = 0; j < face.mNumIndices; j++)
      indices.push_back(face.mIndices[j]);
    }

    // Materials
    Material mat;
    aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];
    aiColor3D color;

    // Material data
    material->Get(AI_MATKEY_COLOR_AMBIENT, color);
    mat._ka = glm::vec3(color.r, color.g, color.b); // ambient
    material->Get(AI_MATKEY_COLOR_DIFFUSE, color);
    mat._kd = glm::vec3(color.r, color.g, color.b); // diffuse
    material->Get(AI_MATKEY_COLOR_SPECULAR, color);
    mat._ks = glm::vec3(color.r, color.g, color.b); // specular

    std::vector<Texture> diffuseMaps = loadMaterialTextures(material,aiTextureType_DIFFUSE, "texture_diffuse");
    textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());

    std::vector<Texture> specularMaps = loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
    textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());

    return Mesh(vertices, indices, textures, mat);
}


GLuint TextureFromFile(const char *path, const std::string &directory){
  std::string filename = std::string(path);
  filename = directory + '/' + filename;
  glimac::FilePath filepath(filename);

  std::unique_ptr<glimac::Image> image = loadImage(filepath);
  if(image == nullptr){
    std::cerr<<"error: image" << filename << "could not be loaded" << std::endl;
    return EXIT_FAILURE;
  }

  GLuint tex;
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D, tex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->getWidth(), image->getHeight(), 0, GL_RGBA, GL_FLOAT, image->getPixels());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glBindTexture(GL_TEXTURE_2D,0);

  return tex;
}


std::vector<Texture> Model::loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName)
{
    std::vector<Texture> textures;
    for(unsigned int i = 0; i < mat->GetTextureCount(type); i++)
    {
        aiString str;
        mat->GetTexture(type, i, &str);
        bool skip = false;
        for(auto& texture : _textures_loaded)
        {
            if(std::strcmp(texture._path.data(), str.C_Str()) == 0)
            {
                textures.push_back(texture);
                skip = true;
                break;
            }
        }
        if(!skip)
        {   // if not loaded, loads the texture
            Texture texture;
            texture._id = TextureFromFile(str.C_Str(), "assets/textures");
            texture._type = typeName;
            texture._path = str.C_Str();
            textures.push_back(texture);
            _textures_loaded.push_back(texture);
        }
    }
    return textures;
}

Model& Model::operator=(const Model& m) noexcept{
    // Ok because done before any OpenGL initialization
    this->_graphical_id = m._graphical_id;
    return *this;
}

void Model::free() {
    for(auto& texture: _textures_loaded)
        glDeleteTextures(1, &texture._id);
    for(auto& mesh: _meshes)
        mesh.free();
}