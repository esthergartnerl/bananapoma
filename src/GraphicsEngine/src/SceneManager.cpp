#include <iostream>
#include "../include/SceneManager.hpp"

std::unordered_map<std::string, std::pair<std::pair<std::string, unsigned int>, std::pair<glm::vec3, glm::vec3>>>
SceneManager::readModelsFromFile(const glimac::FilePath& scene_path) {
    std::ifstream file;
    file.open(scene_path);
    if(!file.is_open()){
        std::cerr << "ERROR::SceneManager : impossible d'ouvrir " << scene_path << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string line;
    std::vector<std::string> s_l;
    std::vector<std::string> pos_string;
    std::vector<std::string> scale_string;
    std::unordered_map<std::string, std::pair<std::pair<std::string, unsigned int>, std::pair<glm::vec3, glm::vec3>>> models;
    unsigned int id = 0;
    std::vector<unsigned int> chosen_indices = {id};
    while (getline(file, line))
    {
        // Read model and type
        boost::split(s_l, line, [](char c){return c == ';';});
        std::string type = s_l.at(0);
        std::string model_path = s_l.at(1);
        // Read pos and scale
        boost::split(pos_string, s_l.at(2), [](char c){return c == ',';});
        boost::split(scale_string, s_l.at(3), [](char c){return c == ',';});
        glm::vec3 pos = {std::stof(pos_string.at(0)), std::stof(pos_string.at(1)),
                         std::stof(pos_string.at(2))};
        glm::vec3 scale = {std::stof(scale_string.at(0)), std::stof(scale_string.at(1)),
                           std::stof(scale_string.at(2))};
        id = 0;
        if(type.find("dynamic") != std::string::npos){
            id = chosen_indices.back() + 1;
            chosen_indices.push_back(id);
        }
        models[type] = {{model_path, id}, {pos, scale}};
    }
    file.close();
    return models;
}

void SceneManager::readLightsFromFile(const glimac::FilePath &lights_path) {
    std::ifstream file;
    file.open(lights_path);
    if(!file.is_open()){
        std::cerr << "ERROR::SceneManager : impossible d'ouvrir " << lights_path << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string line;
    std::vector<std::string> s_l;
    std::vector<std::string> data_1; std::vector<std::string> data_2;
    int uni_id_dir = -1; int uni_id_point = -1;
    while (getline(file, line)){
        boost::split(s_l, line, [](char c){return c == ';';});
        boost::split(data_1, s_l.at(1), [](char c){return c == ',';});
        boost::split(data_2, s_l.at(2), [](char c){return c == ',';});
        if(s_l.at(0) == "directional"){
            glm::vec3 direction = {std::stof(data_1.at(0)), std::stof(data_1.at(1)), std::stof(data_1.at(2))};
            glm::vec3 intensity = {std::stof(data_2.at(0)), std::stof(data_2.at(1)), std::stof(data_2.at(2))};
            _dir_lights.emplace_back(direction, intensity, ++uni_id_dir);
        }
        if(s_l.at(0) == "point"){
            glm::vec3 position = {std::stof(data_1.at(0)), std::stof(data_1.at(1)), std::stof(data_1.at(2))};
            glm::vec3 intensity = {std::stof(data_2.at(0)), std::stof(data_2.at(1)), std::stof(data_2.at(2))};
            _point_lights.emplace_back(position, intensity, ++uni_id_point);
        }
    }
    file.close();
}

void SceneManager::initialize(const uint32_t& sw, const uint32_t& sh, const glimac::FilePath& scene_path,
                              const glimac::FilePath& shaders_path, glimac::Camera& camera) {
    this->readLightsFromFile("assets/lights.txt");
    // Models initialization
    auto models = readModelsFromFile(scene_path);
    for (const auto& model_infos: models) {
        _models[model_infos.first] = {Model(model_infos.second.first.second), model_infos.second.second};
        _models[model_infos.first].first.initialize(model_infos.second.first.first, model_infos.second.second.first);
    }
    // Shaders initialization
    auto shaders_data = ShaderManager::readShadersFromFile(shaders_path);
    for(const auto& data: shaders_data){
        _shaders[data.at(0)] = ShaderManager(data.at(1), data.at(2));
        _shaders[data.at(0)].initialize();
    }
    _screen_width = sw;
    _screen_height = sh;
    _projection_matrix = glm::perspective(glm::radians(70.f), float(sw)/sh, 0.1f, 100.f);
    _camera = &camera;
    _pick_object_max_distance = 4.0; // Only with c3ga
}

void SceneManager::loadResources() {
    for(auto& model: _models) {
        model.second.first.load();
    }
}

void SceneManager::draw(const glm::mat4& view_matrix, const std::string& gra_en_state) {
    _shaders["base"].getProgram().use();
    for(auto& model: _models){ // for every model
        _mv_matrix = view_matrix;
        this->sendLights("base", view_matrix); // send the lights to the fragment shader
        if(model.first.find("dynamic") != std::string::npos){ // id the model is "dynamic"
            // Places the model on the scene
            _mv_matrix = glm::translate(_mv_matrix, model.second.second.first);
            _mv_matrix = glm::scale(_mv_matrix, model.second.second.second);
            sendMatrices("base");
            glEnable(GL_STENCIL_TEST); // Allows the program to draw on the stencil buffer
            model.second.first.draw(_shaders);
        }
        else{
            sendMatrices("base");
            glStencilMask(0x00);
            model.second.first.draw(_shaders);
        }
    }

    // Draw outline:
    if(gra_en_state.find("IN_GAME_CAMERA_UNLOCKED") != std::string::npos){
        _shaders["outline"].getProgram().use();
        for(auto& model: _models){ // for every model
            if(model.first.find("dynamic") != std::string::npos && getSelectedObject(model.second.first, view_matrix) == model.second.first.id()){
                // If the object is selected, draws its outline
                _mv_matrix = view_matrix;
                _mv_matrix = glm::translate(_mv_matrix, model.second.second.first);
                // Resizes it a bit the model to see the outline
                _mv_matrix = glm::scale(_mv_matrix, model.second.second.second + 0.06f*model.second.second.second);
                // little translation because the center point of the models aren't well placed
                _mv_matrix = glm::translate(_mv_matrix, glm::vec3(0.f, -0.007f, 0.f));
                sendMatrices("outline");
                model.second.first.drawOutline();
            }
        }

        glStencilMask(0xFF);
        glStencilFunc(GL_ALWAYS, 0, 0xFF);
    }

    glDisable(GL_STENCIL_TEST);
}

void SceneManager::sendMatrices(const std::string& shader_name) {
    _normal_matrix = glm::transpose(glm::inverse(_mv_matrix));
    glUniformMatrix4fv(_shaders[shader_name].getUniformLocation("uMVPMatrix"), 1, GL_FALSE,
                       glm::value_ptr(_projection_matrix * _mv_matrix));
    glUniformMatrix4fv(_shaders[shader_name].getUniformLocation("uMVMatrix"), 1, GL_FALSE,
                       glm::value_ptr(_mv_matrix));
    glUniformMatrix4fv(_shaders[shader_name].getUniformLocation("uNormalMatrix"), 1, GL_FALSE,
                       glm::value_ptr(_normal_matrix));
}

void SceneManager::sendLights(const std::string& shader_name, const glm::mat4& view_matrix) {
    for(auto& dir_light: _dir_lights)
        dir_light.sendToShader(_shaders[shader_name], view_matrix);
    for(auto& point_light: _point_lights)
        point_light.sendToShader(_shaders[shader_name], view_matrix);
}

GLuint SceneManager::getSelectedObject() const {
    // Reads the value of the pixel in the center of the stencil buffer
    GLuint index;
    GLint x_pos = _screen_width/2.0f;
    GLint y_pos = _screen_height/2.0f;
    glReadPixels(x_pos, y_pos, 1, 1, GL_STENCIL_INDEX, GL_UNSIGNED_INT, &index);
    return index;
}

GLuint SceneManager::getSelectedObject(const glm::mat4& view_matrix) const {
    for(auto& model: _models) {
        if(model.first.find("dynamic") != std::string::npos) {
            if(getSelectedObject(model.second.first, view_matrix) == model.second.first.id()) {
                return model.second.first.id();
            }
        }
    }

    return 0;
}

GLuint SceneManager::getSelectedObject(const Model& model, const glm::mat4& view_matrix) const {
    const double distance = glm::distance(_camera->getPosition(), model._collision_sphere.getCenter());
    if(distance < _pick_object_max_distance) {
        c3ga::Mvec<double> mouse_direction = computeMouseDirection(view_matrix);
        if(model._collision_sphere.intersectLine(mouse_direction)) {
            return model._graphical_id;
        }
    }

    return 0;
}

c3ga::Mvec<double> SceneManager::computeMouseDirection(const glm::mat4& view_matrix) const {
    // Get mouse coordinates in screen space
    const unsigned int mouse_x = _screen_width / 2.0f;
    const unsigned int mouse_y = _screen_height / 2.0f;

    glm::vec3 near_point_world = glm::unProject(glm::vec3(mouse_x, mouse_y, 0.0), view_matrix, _projection_matrix, glm::vec4(0, 0, _screen_width, _screen_height));
    glm::vec3 far_point_world = glm::unProject(glm::vec3(mouse_x, mouse_y, 1.0), view_matrix, _projection_matrix, glm::vec4(0, 0, _screen_width, _screen_height));

    // Create c3ga line
    const c3ga::Mvec<double> start_line = c3ga::point<double>(near_point_world.x, near_point_world.y, near_point_world.z);
    const c3ga::Mvec<double> end_line   = c3ga::point<double>(far_point_world.x, far_point_world.y, far_point_world.z);

    return start_line ^ end_line ^ c3ga::ei<double>();
}

void SceneManager::freeResources() {
    for(auto& model: _models)
        model.second.first.free();
}
