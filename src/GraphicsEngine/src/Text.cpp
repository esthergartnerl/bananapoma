#include "./../include/Text.hpp"

void Text::initialize(std::string text, const float& scale, const glm::vec2& pos, const glm::vec3& color){
    _text = std::move(text), _scale = scale; _pos = pos; _color = color;
    glGenVertexArrays(1, &_vao);
    glGenBuffers(1, &_vbo);
    glBindVertexArray(_vao);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * 4, nullptr, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), nullptr);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void Text::free() {
    glDeleteBuffers(1, &_vbo);
    glDeleteVertexArrays(1, &_vao);
}

void Text::render(const Font& font, ShaderManager& shader, const glm::mat4& projection_matrix) {
    /*
     * Code from : https://learnopengl.com
     */
    // activate corresponding render state
    shader.getProgram().use();
    glUniform3f(shader.getUniformLocation("textColor"), _color.x, _color.y, _color.z);
    glUniformMatrix4fv(shader.getUniformLocation("projection"), 1, GL_FALSE, glm::value_ptr(projection_matrix));
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(_vao);
    float x = _pos.x;
    float y = _pos.y;

    // iterate through all characters
    std::string::const_iterator c;
    for (c = _text.begin(); c != _text.end(); c++)
    {
        Character ch = font.getCharacters()[*c];

        float x_pos = x + ch._bearing.x * _scale;
        float y_pos = y - (ch._size.y - ch._bearing.y) * _scale;

        float w = ch._size.x * _scale;
        float h = ch._size.y * _scale;
        // update VBO for each character
        float vertices[6][4] = {
                { x_pos,     y_pos + h,   0.0f, 0.0f },
                { x_pos,     y_pos,       0.0f, 1.0f },
                { x_pos + w, y_pos,       1.0f, 1.0f },

                { x_pos,     y_pos + h,   0.0f, 0.0f },
                { x_pos + w, y_pos,       1.0f, 1.0f },
                { x_pos + w, y_pos + h,   1.0f, 0.0f }
        };
        // render glyph texture over quad
        glBindTexture(GL_TEXTURE_2D, ch._textureId);
        // update content of VBO memory
        glBindBuffer(GL_ARRAY_BUFFER, _vbo);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        // render quad
        glDrawArrays(GL_TRIANGLES, 0, 6);
        // now advance cursors for next glyph (note that advance is number of 1/64 pixels)
        x += (ch._advance >> 6) * _scale; // bitshift by 6 to get value in pixels (2^6 = 64)
    }
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}