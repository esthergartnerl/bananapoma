#include "./../include/GraphicalUserInterface.hpp"

void GraphicalUserInterface::initialize(CookingPot& cp, const uint32_t& sw, const uint32_t& sh){
    // Shaders initialization
    auto shaders_data = ShaderManager::readShadersFromFile("assets/gui_shaders.txt");
    for(const auto& data: shaders_data){
        _shaders[data.at(0)] = ShaderManager(data.at(1), data.at(2));
        _shaders[data.at(0)].initialize();
    }

    _projection_matrix = glm::ortho(0.f, static_cast<float>(sw), 0.f, static_cast<float>(sh));
    _cooking_pot_disp.initialize(cp, _shaders["glyph_shader"], sh);
    _main_menu.initialize(_shaders["glyph_shader"]);
    _pause_menu.initialize(_shaders["glyph_shader"]);
    _selected_ing.initialize("", 0.5, {sw - 300, 25}, glm::vec3(1.0f));

    // Main menu buttons :
    glm::vec2 p_b = {120, 50}; glm::vec3 p_t = {0.5, sw - 140, 30}; glm::vec3 color = {1.0f, 1.0f, 1.0f};
    std::string action;
    _main_menu.addButton("quit", "QUITTER", action, p_b, p_t, color);
    p_b = {140, 60}; p_t = {0.8, sw/2.0f - 50, sh/2.0f - 10}; color = {1.0f, 1.0f, 1.0f};
    action = "SCENE IN_GAME_GUI IN_GAME_CAMERA_UNLOCKED";
    _main_menu.addButton("play", "JOUER", action , p_b, p_t, color);
    // Pause menu buttons :
    p_b = {300, 60}; p_t = {0.8, sw/2.0f - 120, sh/2.0f - 10}; color = {1.0f, 1.0f, 1.0f};
    action = "SCENE IN_GAME_GUI IN_GAME_CAMERA_UNLOCKED";
    _pause_menu.addButton("resume", "RETOUR AU JEU", action, p_b, p_t, color);
    p_b = {140, 60}; p_t = {0.8, sw/2.0f - 40, sh/2.0f - 100}; color = {1.0f, 1.0f, 1.0f};
    action = "SCENE MENU_GUI MENU_CAMERA";
    _pause_menu.addButton("back_menu", "MENU", action, p_b, p_t, color);
}

void GraphicalUserInterface::loadResources() {
    _font.load("assets/fonts/playtime.ttf");
}

void GraphicalUserInterface::draw(const std::string& state) {
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    if(state.find("MENU_GUI") != std::string::npos)
        _main_menu.draw(_font, _projection_matrix);
    if(state.find("IN_GAME_PAUSE_GUI") != std::string::npos)
        _pause_menu.draw(_font, _projection_matrix);
    if(state.find("IN_GAME_GUI") != std::string::npos){
        _cooking_pot_disp.draw(_font, _projection_matrix);
        if(_selected_ing != "")
            _selected_ing.render(_font, _shaders["glyph_shader"], _projection_matrix);
    }
}

void GraphicalUserInterface::freeResources() {
    _font.free();
    _selected_ing.free();
    _cooking_pot_disp.free();
    _main_menu.free();
    _pause_menu.free();
}
