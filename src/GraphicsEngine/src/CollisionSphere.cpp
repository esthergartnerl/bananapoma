#include "../include/CollisionSphere.hpp"

CollisionSphere::CollisionSphere(const glm::vec3& center, const double& radius): _center(center), _radius(radius) {
    // Create the sphere using 4 random points at radius distance from the center
    c3ga::Mvec<double> baseSphere = createSphere(radius);

    // Translate the sphere to its center (in world coordinates)
    c3ga::Mvec<double> TX = 1.0f - 0.5f * center.x * c3ga::e1i<double>(); // Translator X
    c3ga::Mvec<double> TY = 1.0f - 0.5f * center.y * c3ga::e2i<double>(); // Translator Y
    c3ga::Mvec<double> TZ = 1.0f - 0.5f * center.z * c3ga::e3i<double>(); // Translator Z
    baseSphere = TX * baseSphere * TX.inv();
    baseSphere = TY * baseSphere * TY.inv();
    _sphere    = TZ * baseSphere * TZ.inv();
}

CollisionSphere::CollisionSphere() {
    _radius = 1.0;
    _center = glm::vec3(0.0);
    _sphere = createSphere(_radius);
}

void CollisionSphere::initialize(const glm::vec3& center, const double& radius) {
    *this = CollisionSphere(center, radius);
}

c3ga::Mvec<double> CollisionSphere::createSphere(const double& radius) {
    // Tools for random generation
    std::default_random_engine generator;
    generator.seed(time(NULL));
    std::uniform_real_distribution<double> urDistrib_m1_1(-1.0f, 1.0f);
    std::uniform_real_distribution<double> urDistrib_0_2pi(0.0f, 2 * M_PI);

    // Generate 4 points on a sphere
    // Formula taken here: https://askcodez.com/dispersion-de-n-points-uniformement-sur-une-sphere.html
    std::vector<c3ga::Mvec<double>> points;
    for(int i = 0; i < 4; ++i) {
        const double t = urDistrib_0_2pi(generator); // Angle
        const double z = urDistrib_m1_1(generator);
        const double y = std::sqrt(1.0f - std::pow(z, 2)) * std::sin(t);
        const double x = std::sqrt(1.0f - std::pow(z, 2)) * std::cos(t);
        points.push_back(c3ga::point<double>(x * radius, y * radius, z * radius));
    }

    return points[0] ^ points[1] ^ points[2] ^ points[3];
}

bool CollisionSphere::intersectLine(const c3ga::Mvec<double>& line) const {
    // Line sphere intersection
    c3ga::Mvec<double> intersection = !line ^ !_sphere;

    // The result is a pair of points
    c3ga::Mvec<double> pointA, pointB;
    c3ga::extractPairPoint(!intersection, pointA, pointB);
    
    // We only need to do the computation on one of the points
    const glm::vec3 point = glm::vec3(pointA[c3ga::E1], pointA[c3ga::E2], pointA[c3ga::E3]);
    return glm::distance(point, _center) < _radius + 0.01; // Small value which let a little interval for the comparison
}