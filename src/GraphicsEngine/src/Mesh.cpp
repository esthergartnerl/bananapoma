#include "../include/Mesh.hpp"

void Mesh::setupMesh(){
    /*
     * Code from : https://learnopengl.com
     * Modified
     */
    glGenVertexArrays(1, &_vao);
    glGenBuffers(1, &_vbo);
    glGenBuffers(1, &_ebo);

    glBindVertexArray(_vao);

    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(GL_ARRAY_BUFFER, _vertices.size() * sizeof(Vertex), _vertices.data(), GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, _indices.size() * sizeof(unsigned int), _indices.data(), GL_STATIC_DRAW);
    // Vertices positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, _position));
    // Normals
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, _normal));
    // Texture coordinates
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, _tex_coords));

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
  }

void Mesh::draw(std::unordered_map<std::string, ShaderManager>& shaders, const unsigned int& graphical_id){
    /*
     * Code from : https://learnopengl.com
     */
    unsigned int diffuseNr = 1;
    unsigned int specularNr = 1;
    GLuint base_shader_id = shaders["base"].getProgram().getGLId();
    for(unsigned int i = 0; i < _textures.size(); i++)
    {
        glActiveTexture(GL_TEXTURE0 + i);
        std::string number;
        std::string name = _textures[i]._type;
        if(name == "texture_diffuse") number = std::to_string(diffuseNr++);
        else if(name == "texture_specular") number = std::to_string(specularNr++);

        glUniform1f(glGetUniformLocation(base_shader_id, ("material." + name + number).c_str()), i);
        glBindTexture(GL_TEXTURE_2D, _textures[i]._id);
    }
    // affiche le mesh
    glBindVertexArray(_vao);
    glUniform3f(shaders["base"].getUniformLocation("uAmbient"), _mats._ka[0], _mats._ka[1], _mats._ka[2]);
    glUniform3f(shaders["base"].getUniformLocation("uDiffuse"), _mats._kd[0], _mats._kd[1], _mats._kd[2]);
    glUniform3f(shaders["base"].getUniformLocation("uSpecular"), _mats._ks[0], _mats._ks[1], _mats._ks[2]);
    if(graphical_id > 0){ // Checks if a "dynamic" model is being drawn
        glStencilFunc(GL_ALWAYS, graphical_id, 0xFF);
        glStencilMask(0xFF);
    }
    glDrawElements(GL_TRIANGLES, _indices.size(), GL_UNSIGNED_INT, nullptr);
    for(unsigned int i = 0; i < _textures.size(); i++)
    {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    glBindVertexArray(0);
}

void Mesh::drawOutline(const unsigned int& graphical_id) const{
    glBindVertexArray(_vao);
    glStencilFunc(GL_NOTEQUAL, graphical_id, 0xFF);
    glStencilMask(0x00);
    glDrawElements(GL_TRIANGLES, _indices.size(), GL_UNSIGNED_INT, nullptr);
    glBindVertexArray(0);
}

void Mesh::free() {
    glDeleteBuffers(1, &_vbo);
    glDeleteBuffers(1, &_vao);
    glDeleteBuffers(1, &_ebo);
}


