#include "./../include/ShaderManager.hpp"

GLint ShaderManager::getUniformLocation(const std::string &location_name) const {
    /*
     * Marked as const because the main purpose of this method is to return the uniform location
     * and not to add the location into the cache.
     * Code from : https://www.youtube.com/watch?v=nBB0LGSIm5Q (The Cherno)
     */
    if(_uniform_location_cache.find(location_name) != _uniform_location_cache.end())
        return _uniform_location_cache[location_name];

    GLint location = glGetUniformLocation(_program.getGLId(), location_name.c_str());
    _uniform_location_cache[location_name] = location;
    return location;
}

ShaderManager& ShaderManager::operator=(const ShaderManager& s_m) noexcept {
    // Ok because used before any OpenGL initialization
    this->_vs_path = s_m._vs_path;
    this->_fs_path = s_m._fs_path;
    return *this;
}

std::vector<std::vector<std::string>> ShaderManager::readShadersFromFile(const glimac::FilePath& file_path){
    std::ifstream file;
    file.open(file_path);
    if(!file.is_open()){
        std::cerr << "ERROR::SceneManager : impossible d'ouvrir " << file_path << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string line;
    std::vector<std::string> splitted_line;
    std::vector<std::vector<std::string>> shaders_data;
    while (getline(file, line))
    {
        boost::split(splitted_line, line, [](char c){return c == ';';});
        shaders_data.push_back({splitted_line[0], splitted_line[1], splitted_line[2]});
    }
    file.close();
    return shaders_data;
}
