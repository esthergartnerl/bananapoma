#include "../include/Menu.hpp"

void Menu::initialize(ShaderManager& text_shader) {
    _text_shader = &text_shader;
}

/*
 * Add a button to the menu :
 * n = id_name, t = title, a = action, p_b = width and height of button,
 * p_t = scale, x, y of text, color = color of button
 */
void Menu::addButton(const std::string& n, const std::string& t, const std::string& a, const glm::vec2& p_b,
                     const glm::vec3& p_t, const glm::vec3& color) {
    _buttons.emplace(n, std::pair<Button, Text>(Button(p_t.y - 20, p_t.z - 20, p_b.x, p_b.y, a), Text()));
    _buttons[n].second.initialize(t, p_t.x,glm::vec2(p_t.y, p_t.z), color);
}

void Menu::draw(const Font &font, const glm::mat4 &projection_matrix) {
    for(auto& button: _buttons){
        if(button.second.first.isHovered())
            button.second.second.setColor(glm::vec3(0.95, 0.2, 0.1));
        else
            button.second.second.setColor(glm::vec3(1.0));
        button.second.second.render(font, *_text_shader, projection_matrix);
    }
}

void Menu::free() {
    for(auto& button: _buttons)
        button.second.second.free();
}

void Menu::checkButtons(std::string& engine_state) const{
    for(auto& button: _buttons)
        if(button.second.first.isHovered())
            engine_state = button.second.first.getAction();
}