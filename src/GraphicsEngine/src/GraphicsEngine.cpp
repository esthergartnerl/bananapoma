#include "./../include/GraphicsEngine.hpp"
#include "../../EventsManager.hpp"

GraphicsEngine::GraphicsEngine(const uint32_t& sw, const uint32_t sh):_screen_width(sw), _screen_height(sh), _camera(),
_menu_camera(5.f, {35.f, 0.0f}), _gui(), _state(){}

void GraphicsEngine::initialize(CookingPot& cp, const unsigned int& frame_rate) {
#ifdef VERBOSE
    std::cout << "Initialisation du moteur graphique" << std::endl;
#endif
    // Initialize glew for OpenGL3+ support
    GLenum glew_init_error = glewInit();
    if(GLEW_OK != glew_init_error) {
        std::cerr << glewGetErrorString(glew_init_error) << std::endl;
        exit(EXIT_FAILURE);
    }
#ifdef VERBOSE
    std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;
#endif
    _state = "SCENE MENU_GUI MENU_CAMERA";
    _camera.setSpeed(2.5f);
    _camera.setPosition({5.f, 0.75f, 25.f});
    _menu_camera.setPosition({-3.f, -1.5f, -25.5f});
    _gui.initialize(cp, _screen_width, _screen_height);
    _scene.initialize(_screen_width, _screen_height, "assets/scene.txt", "assets/scene_shaders.txt", _camera);
    _fps_manager.initialize(frame_rate);
}

void GraphicsEngine::loadResources() {
#ifdef VERBOSE
    std::cout << "Chargement des ressources du moteur graphique" << std::endl;
#endif
    _gui.loadResources();
    _scene.loadResources();
}

void GraphicsEngine::draw(glimac::SDLWindowManager& window_manager) {
    glClearColor(0.4f, 0.55f, 0.85f, 1.0f);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    // 3D scene rendering
    if(_state.find("SCENE") != std::string::npos){
        glDisable(GL_BLEND);
        glEnable(GL_DEPTH_TEST);
        if(_state.find("MENU_CAMERA") != std::string::npos)
            _scene.draw(_menu_camera.getViewMatrix(), _state);
        if(_state.find("IN_GAME_CAMERA") != std::string::npos)
            _scene.draw(_camera.getViewMatrix(), _state);
    }

    // 2D elements rendering
    glDisable(GL_DEPTH_TEST);
    _gui.draw(_state);

    if(_state.find("SCENE") != std::string::npos){
        if(_state.find("MENU_CAMERA") != std::string::npos)
            _menu_camera.rotateAroundPoint({0, 0.5}, 1.0f/_fps_manager.averageFps());
        if(_state.find("IN_GAME_CAMERA_UNLOCKED") != std::string::npos){
            _camera.updatePosition(1.0f/_fps_manager.averageFps());
            SDL_WarpMouse(_screen_width/2.0f, _screen_height/2.0f); // center the mouse on the screen
        }
    }
    window_manager.swapBuffers();
    _fps_manager.limitFps(window_manager);
#ifdef VERBOSE
    //std::cout << "AVERAGE FPS : " << _fps_manager.averageFps() << std::endl;
#endif
}

void GraphicsEngine::handleEvents(EventsManager &events_manager) {
    float x_pos; float y_pos;
    switch (events_manager.event().type) {
        case SDL_MOUSEBUTTONDOWN:
            if(events_manager.event().button.button == SDL_BUTTON_LEFT){
                // Checks every button of the guis to update the state of the GraphicsEngine
                if(_state.find("MENU_GUI") != std::string::npos)
                    _gui.getMainMenu().checkButtons(_state);
                if(_state.find("IN_GAME_PAUSE_GUI") != std::string::npos)
                    _gui.getPauseMenu().checkButtons(_state);
            }
            break;
        case SDL_MOUSEMOTION:
            x_pos = events_manager.event().motion.x;
            y_pos = float(_screen_height) - float(events_manager.event().motion.y);
            // Updates every button of the guis
            if(_state.find("MENU_GUI") != std::string::npos)
                for(auto& button: _gui.getMainMenuButtons())
                    button.second.first.setHover(x_pos, y_pos);
            if(_state.find("IN_GAME_PAUSE_GUI") != std::string::npos)
                for(auto& button: _gui.getPauseMenuButtons())
                    button.second.first.setHover(x_pos, y_pos);
            // Changes the camera orientation using the mouse
            if(_state.find("IN_GAME_CAMERA_UNLOCKED") != std::string::npos){
                x_pos = 0.1f * float(_screen_width/2.0f - float(events_manager.event().motion.x));
                y_pos = 0.1f * float(_screen_height/2.0f - float(events_manager.event().motion.y));
                _camera.rotateFront(y_pos);
                _camera.rotateLeft(x_pos);
            }
            break;
        default:
            break;
    }
}

void GraphicsEngine::freeResources() {
#ifdef VERBOSE
    std::cout << "Libération des ressources du moteur graphique" << std::endl;
#endif
    _gui.freeResources();
    _scene.freeResources();
}

