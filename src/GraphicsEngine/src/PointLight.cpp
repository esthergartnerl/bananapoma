#include "../include/PointLight.hpp"

void PointLight::sendToShader(const ShaderManager& shader, const glm::mat4& view_matrix) {
    this->computePosition(view_matrix);
    glUniform3fv(shader.getUniformLocation("point_lights[" + std::to_string(_uniform_id) + "].position"),
                 1, glm::value_ptr(_computed_position));
    glUniform3fv(shader.getUniformLocation("point_lights[" + std::to_string(_uniform_id) + "].intensity"),
                 1, glm::value_ptr(_intensity));
}

