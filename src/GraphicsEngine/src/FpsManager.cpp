#include "../include/FpsManager.hpp"

void FpsManager::initialize(const unsigned int& frame_rate) {
    setFrameRate(frame_rate);
    _frame_times = std::vector<uint32_t>(_frame_rate);
    _frame_count = 0;
    _frame_time_last = 0;
#ifdef VERBOSE
    std::cout << "FPS : " << _frame_rate << std::endl;
#endif
}

void FpsManager::setFrameRate(const unsigned int &fr) {
    _frame_rate = fr;
    _frame_delay = 1e3f / float(fr);
}

void FpsManager::limitFps(const glimac::SDLWindowManager& window_manager) {
    // Get current time
    float now = window_manager.getTicks();
    // Check if the time to make one frame is lower than the requested frame rate
    if(_frame_delay > (now - _frame_time_last))
        SDL_Delay(_frame_delay - (now - _frame_time_last));
    // Time checking after the delay
    now = window_manager.getTicks();
    uint32_t frame_times_index = _frame_count % _frame_rate;
    _frame_times.at(frame_times_index) = now - _frame_time_last;
    // Stores the current time to compare with next frame time
    _frame_time_last = now;
    ++_frame_count;
}

float FpsManager::averageFps() const {
    float count;
    if (_frame_count < _frame_rate)
        count = _frame_count;
    else
        count = float(_frame_rate);

    float fps = 0;
    for (auto& frame_time: _frame_times)
        fps += float(frame_time);

    fps /= count;
    return 1e3f / fps;
}