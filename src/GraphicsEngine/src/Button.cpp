#include "../include/Button.hpp"

Button::Button(const Button& btn) {
    _pos_x = btn._pos_x;
    _pos_y = btn._pos_y;
    _width = btn._width;
    _height = btn._height;
    _hover = false;
    _action = btn._action;
}

void Button::setHover(const float& mouse_x, const float& mouse_y){
    // Checks if the button is hovered
    bool condition_x = mouse_x >= _pos_x && mouse_x <= _pos_x + _width;
    bool condition_y = mouse_y >= _pos_y && mouse_y <= _pos_y + _height;
    if(condition_x && condition_y)
        _hover = true;
    else
        _hover = false;
}