#include "../include/CookingPotDisplay.hpp"

void CookingPotDisplay::initialize(CookingPot& cp, ShaderManager& text_shader, const uint32_t& sh) {
    _screen_height = sh;
    _text_shader = &text_shader;
    _cooking_pot = &cp;
    _cooking_pot->registerObserver(*this);
    _title.initialize("Liste des ingrédients", 0.45, glm::vec2(25, sh - 30), glm::vec3(1.0));
    _recipe_info.initialize("", 0.42, glm::vec2(25, 25), glm::vec3(1.0));
}

void CookingPotDisplay::update() {
    if(!_cooking_pot->getIngredients().empty()){ // When addIngredient() is triggered
        _first_ingredient = true;
        Ingredient new_ing = _cooking_pot->getIngredients().back();
        float line_spacing = 20.f*_content.size() + 30.f;
        _content.emplace_back(Text());
        _content.back().initialize(new_ing.getName(), 0.35, glm::vec2(25, _screen_height - 25.f - line_spacing), glm::vec3(1.));
    }
    else{ // When cook() is triggered
        _first_recipe = true;
        const Recipe* last_recipe = _cooking_pot->getLastRecipe();
        if(last_recipe != nullptr)
            _recipe_info.setText("Derniere recette :  " + last_recipe->getName());
        else
            _recipe_info.setText("Derniere recette : ratee !");

        for(auto& text: _content)
            text.free();
        _content.clear();
    }
}

void CookingPotDisplay::draw(const Font& font, const glm::mat4& projection_matrix) {
    // Draw the content of the cooking pot only if it's not empty or if the player has already cooked something
    if(_first_recipe || _first_ingredient){
        _title.render(font, *_text_shader, projection_matrix);
        _recipe_info.render(font, *_text_shader, projection_matrix);
        for(auto& text: _content)
            text.render(font, *_text_shader, projection_matrix);
    }
}

void CookingPotDisplay::free() {
    _cooking_pot->unregisterObserver(*this);
    _title.free();
    _recipe_info.free();
    for(auto& text: _content)
        text.free();
}