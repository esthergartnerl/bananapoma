#include "../include/DirectionalLight.hpp"

void DirectionalLight::sendToShader(const ShaderManager& shader, const glm::mat4& view_matrix) {
    this->computeDirection(view_matrix);
    glUniform3fv(shader.getUniformLocation("directional_lights[" + std::to_string(_uniform_id) + "].direction"),
                 1, glm::value_ptr(_computed_direction));
    glUniform3fv(shader.getUniformLocation("directional_lights[" + std::to_string(_uniform_id) + "].intensity"),
                 1, glm::value_ptr(_intensity));
}