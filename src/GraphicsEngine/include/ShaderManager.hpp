#pragma once
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include <iostream>
#include <unordered_map>
#include <vector>
#include <fstream>
#include <boost/algorithm/string.hpp>

/*
 * Gives the source code of the vertex shader (situated here : _vs_path) and the fragment shader (_fs_path) to OpenGL
 * using the glimac library. Then these shaders are compiled and ready to be used (using lib glimac)
 *
 */

class ShaderManager{
private:
    glimac::Program _program;
    glimac::FilePath _vs_path;
    glimac::FilePath _fs_path;
    mutable std::unordered_map<std::string, GLint> _uniform_location_cache;

public:
    ShaderManager(glimac::FilePath vs = "", glimac::FilePath fs = ""):_program(), _vs_path(vs), _fs_path(fs){};
    ~ShaderManager() = default;
    ShaderManager& operator=(const ShaderManager& s_m) noexcept;

    inline void initialize(){_program = glimac::loadProgram(_vs_path, _fs_path);};
    static std::vector<std::vector<std::string>> readShadersFromFile(const glimac::FilePath& file_path);

    GLint getUniformLocation(const std::string& location_name) const;
    inline glimac::Program& getProgram(){return _program;};
};
