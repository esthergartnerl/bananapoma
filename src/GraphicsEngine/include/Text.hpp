#pragma once
#include "ShaderManager.hpp"
#include "Font.hpp"
#include <utility>

/*
 * Represents a text.
 * Manages all the OpenGL tasks needed to render a text.
 */

class Text{
private:
    GLuint _vao;
    GLuint _vbo;
    std::string _text;
    float _scale;
    glm::vec2 _pos;
    glm::vec3 _color;
public:
    Text() = default;
    ~Text() = default;

    void initialize(std::string text, const float& scale = 1.0, const glm::vec2& pos = glm::vec2(0.),
                    const glm::vec3& color = glm::vec3(1.));
    void render(const Font& font, ShaderManager& shader, const glm::mat4& projection_matrix);
    void free();

    inline void setText(std::string new_text){_text = std::move(new_text);};
    inline void setColor(const glm::vec3& new_color){_color = new_color;};

    inline bool operator!=(const std::string& text)const noexcept{return text != _text;};
};

