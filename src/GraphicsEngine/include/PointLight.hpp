#pragma once
#include <glimac/glm.hpp>
#include <string>
#include <unordered_map>
#include "ShaderManager.hpp"
#include <vector>

/*
 * Represents a point light
 */

class PointLight{
private:
    glm::vec3 _position;
    glm::vec3 _intensity;
    glm::vec3 _computed_position;
    int _uniform_id;

    inline void computePosition(const glm::mat4& view_matrix)
    {_computed_position = glm::vec3(view_matrix * glm::vec4(_position, 1.f));};
public:
    PointLight(const glm::vec3& pos, const glm::vec3& intensity, const int& uni_id):
    _position(pos), _intensity(intensity), _uniform_id(uni_id){};
    ~PointLight() = default;

    void sendToShader(const ShaderManager& shader, const glm::mat4& view_matrix);
};

