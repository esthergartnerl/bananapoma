#pragma once
#include <glimac/glm.hpp>
#include <iostream>
#include <vector>
#include <glimac/Program.hpp>
#include "ShaderManager.hpp"

/*
 * Code from : https://learnopengl.com
 * Modified
 */

struct Texture {
    unsigned int _id;
    std::string _type;
    std::string _path;
};

struct Vertex {
    glm::vec3 _position;
    glm::vec3 _normal;
    glm::vec2 _tex_coords;
};

struct Material {
    glm::vec3 _ka;
    glm::vec3 _kd;
    glm::vec3 _ks;
};

class Mesh {
public:
    std::vector<Vertex> _vertices;
    std::vector<unsigned int> _indices;
    std::vector<Texture> _textures; // tex coords
    GLuint _vao, _vbo, _ebo;
    Material _mats;

    Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures, Material mat){
      this->_vertices = vertices;
      this->_indices = indices;
      this->_textures = textures;
      this->_mats = mat;
      setupMesh();
    };

    void setupMesh();
    void free();

    void draw(std::unordered_map<std::string, ShaderManager>& shaders, const unsigned int& graphical_id);
    void drawOutline(const unsigned int& graphical_id) const;
};
