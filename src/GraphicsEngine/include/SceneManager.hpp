#pragma once
#include <glimac/Camera.hpp>
#include <iostream>
#include <unordered_map>
#include "ShaderManager.hpp"
#include "Model.hpp"
#include <fstream>
#include "DirectionalLight.hpp"
#include "PointLight.hpp"

/*
 * This class manages all the data needed to draw the 3D scene.
 * _shaders : [key] --> ShaderManager
 * _models : [key] --> {Model, {pos, scale}}
 */

class SceneManager{
private:
    std::unordered_map<std::string, ShaderManager> _shaders;
    std::unordered_map<std::string, std::pair<Model, std::pair<glm::vec3, glm::vec3>>> _models;
    std::vector<DirectionalLight> _dir_lights;
    std::vector<PointLight> _point_lights;
    glm::mat4 _projection_matrix;
    glm::mat4 _mv_matrix;
    glm::mat4 _normal_matrix;
    uint32_t _screen_width;
    uint32_t _screen_height;
    glimac::Camera* _camera;
    double _pick_object_max_distance;

    static std::unordered_map<std::string, std::pair<std::pair<std::string, unsigned int>, std::pair<glm::vec3, glm::vec3>>>
    readModelsFromFile(const glimac::FilePath& scene_path);
    void readLightsFromFile(const glimac::FilePath& lights_path);
    c3ga::Mvec<double> computeMouseDirection(const glm::mat4& view_matrix) const;
public:
    SceneManager() = default;
    ~SceneManager() = default;

    void initialize(const uint32_t& sw, const uint32_t& sh, const glimac::FilePath& scene_path,
                    const glimac::FilePath& shaders_path, glimac::Camera& camera);
    void loadResources();
    void draw(const glm::mat4& view_matrix, const std::string& gra_en_state);
    void freeResources();

    void sendMatrices(const std::string& shader_name);
    void sendLights(const std::string& shader_name, const glm::mat4& view_matrix);

    GLuint getSelectedObject() const; // Using the stencil buffer
    GLuint getSelectedObject(const Model& model, const glm::mat4& view_matrix) const; // Using c3ga
    GLuint getSelectedObject(const glm::mat4& view_matrix) const; // Using c3ga
};
