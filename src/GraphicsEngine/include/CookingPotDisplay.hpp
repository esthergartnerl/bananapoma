#pragma once
#include "Text.hpp"
#include <vector>
#include "../../Observer.hpp"
#include "../../GameEngine/include/CookingPot.hpp"

/*
 * Displays the status of the cooking pot (has smth in it, last recipe ...)
 */

class CookingPotDisplay: public Observer{
private:
    uint32_t _screen_height;
    ShaderManager* _text_shader;
    CookingPot* _cooking_pot;
    Text _title;
    Text _recipe_info;
    std::vector<Text> _content;
    bool _first_recipe;
    bool _first_ingredient;
public:
    CookingPotDisplay():_screen_height(), _text_shader(nullptr), _cooking_pot(nullptr),_first_recipe(false),
    _first_ingredient(false){};
    ~CookingPotDisplay() override = default;

    void initialize(CookingPot& cp, ShaderManager& text_shader, const uint32_t& sh);
    void update() override;
    void draw(const Font& font, const glm::mat4& projection_matrix);
    void free();
};