#pragma once
#include <glimac/glm.hpp>
#include <c3ga/Mvec.hpp>
#include "../../c3gaTools.hpp"

/*
 * Collision sphere using c3ga
 */

class CollisionSphere {
private:
    c3ga::Mvec<double> _sphere;
    glm::vec3 _center;
    double _radius;

    c3ga::Mvec<double> createSphere(const double& radius);
public:
    CollisionSphere(const glm::vec3& center, const double& radius);
    CollisionSphere();
    ~CollisionSphere() = default;

    void initialize(const glm::vec3& center, const double& radius);
    bool intersectLine(const c3ga::Mvec<double>& line) const;

    const glm::vec3& getCenter() const { return _center; }
};