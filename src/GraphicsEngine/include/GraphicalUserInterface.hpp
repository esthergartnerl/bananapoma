#pragma once
#include <glimac/SDLWindowManager.hpp>
#include <glimac/Program.hpp>
#include <iostream>
#include "ShaderManager.hpp"
#include "Text.hpp"
#include "CookingPotDisplay.hpp"
#include "Menu.hpp"

/*
 * Main class of the 2D GUI.
 * Is in charge of rendering the 2D GUI.
 * Contains the main resources to render the GUI.
 * Its role is also to divide the task to other components.
 */

class GraphicalUserInterface{
private:
    std::unordered_map<std::string, ShaderManager> _shaders;
    glm::mat4 _projection_matrix;
    CookingPotDisplay _cooking_pot_disp;
    Font _font;
    Menu _main_menu;
    Menu _pause_menu;
    Text _selected_ing;
public:
    GraphicalUserInterface() = default;
    ~GraphicalUserInterface() = default;

    void initialize(CookingPot& cp, const uint32_t& sw, const uint32_t& sh);
    void loadResources();
    void draw(const std::string& state);
    void freeResources();

    inline void setSelectedIng(const std::string& selected_ing){_selected_ing.setText(selected_ing);};

    inline auto& getMainMenuButtons(){return _main_menu.getButtons();};
    inline auto& getPauseMenuButtons(){return _pause_menu.getButtons();};
    inline Menu& getMainMenu(){return _main_menu;};
    inline Menu& getPauseMenu(){return _pause_menu;};
};