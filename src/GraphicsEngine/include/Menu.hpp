#pragma once

#include <string>
#include <map>
#include "Button.hpp"
#include "Text.hpp"

class Menu{
private:
    ShaderManager* _text_shader;
    std::map<std::string, std::pair<Button, Text>> _buttons;
public:
    Menu():_text_shader(nullptr){};
    virtual ~Menu()= default;

    void initialize(ShaderManager& text_shader);
    void addButton(const std::string& n, const std::string& t, const std::string& a, const glm::vec2& p_b,
                   const glm::vec3& p_t, const glm::vec3& color);
    void draw(const Font& font, const glm::mat4& projection_matrix);
    void checkButtons(std::string& engine_state) const;
    void free();

    inline std::map<std::string, std::pair<Button, Text>>& getButtons(){return _buttons;};
};