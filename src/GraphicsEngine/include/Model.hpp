#pragma once
#include <glimac/glm.hpp>
#include <iostream>
#include <vector>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <glimac/Program.hpp>
#include "Mesh.hpp"
#include <glimac/Image.hpp>
#include "../include/CollisionSphere.hpp"

/*
 * Code from : https://learnopengl.com
 * Modified
 */

class Model{
public:
    std::vector<Mesh> _meshes;
    std::vector<Texture> _textures_loaded;
    std::string _path;
    unsigned int _graphical_id;
    CollisionSphere _collision_sphere;

    Model(const unsigned int& gra_id = 0):_graphical_id(gra_id), _collision_sphere() {}
    ~Model() = default;
    Model& operator=(const Model& m) noexcept;

    void draw(std::unordered_map<std::string, ShaderManager>& shaders);
    void drawOutline();
    void initialize(const std::string& pathModel, const glm::vec3& position);
    void load();
    void free();

    void processNode(aiNode *node, const aiScene *scene);
    Mesh processMesh(aiMesh *mesh, const aiScene *scene);
    std::vector<Texture> loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName);

    inline unsigned int id() const{return _graphical_id;};
};
