#pragma once
#include <SDL/SDL.h>
#include <vector>
#include <iostream>
#include <glimac/SDLWindowManager.hpp>

/*
 * Tool to limit the fps. Can also give the current average fps.
 * Class inspired by this code : http://sdl.beuc.net/sdl.wiki/SDL_Average_FPS_Measurement
 */

class FpsManager{
private:
    std::vector<uint32_t> _frame_times;
    uint32_t _frame_count;
    unsigned int _frame_rate;
    float _frame_delay;
    float _frame_time_last;
public:
    FpsManager() = default;
    ~FpsManager() = default;

    void initialize(const unsigned int& frame_rate);
    void limitFps(const glimac::SDLWindowManager& window_manager);

    void setFrameRate(const unsigned int& fr);

    float averageFps() const;
};