#pragma once
#include <glimac/glm.hpp>
#include <string>
#include <unordered_map>
#include "ShaderManager.hpp"
#include <vector>

/*
 * Represents a directional light
 */

class DirectionalLight{
private:
    glm::vec3 _direction;
    glm::vec3 _intensity;
    glm::vec3 _computed_direction;
    int _uniform_id;

    inline void computeDirection(const glm::mat4& view_matrix)
    {_computed_direction = glm::vec3(view_matrix * glm::vec4(_direction, 0.f));};
public:
    DirectionalLight(const glm::vec3& dir, const glm::vec3& intensity, const int& uni_id):
    _direction(dir), _intensity(intensity), _uniform_id(uni_id){};
    ~DirectionalLight() = default;

    void sendToShader(const ShaderManager& shader, const glm::mat4& view_matrix);
};
