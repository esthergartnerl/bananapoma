#pragma once
#include <string>
#include <utility>

/*
 * Represents a button in the GUI
 */

class Button{
private:
    float _pos_x;
    float _pos_y;
    float _width;
    float _height;
    bool _hover;
    std::string _action;
public:
    explicit Button(const float& x = 0, const float& y = 0, const float& w = 0, const float& h = 0, std::string  action = ""):
    _pos_x(x), _pos_y(y), _width(w), _height(h), _hover(false), _action(std::move(action)){};
    Button(const Button& btn);
    ~Button() = default;

    void setHover(const float& mouse_x, const float& mouse_y);

    inline bool isHovered() const noexcept{return _hover;};
    inline std::string getAction() const noexcept{return _action;};
};