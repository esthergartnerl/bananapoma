#pragma once
#include <glimac/glm.hpp>
#include <glimac/FilePath.hpp>
#include <map>
#include <string>
#include <iostream>
#include <GL/glew.h>
#include <ft2build.h>
#include FT_FREETYPE_H

struct Character{
    GLuint _textureId;
    glm::ivec2 _size;
    glm::ivec2 _bearing;
    FT_Pos _advance;
};

/*
 * This class stores all the characters of a font converted into textures
 */

class Font{
private:
    std::map<char, Character> _characters;
    glimac::FilePath _file_path;
public:
    Font(const glimac::FilePath& fp = ""):_file_path(fp){};
    ~Font() = default;

    void load(const glimac::FilePath& fp);
    void free();

    inline std::map<char, Character> getCharacters() const noexcept {return _characters;};
};
