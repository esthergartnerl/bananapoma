#pragma once
#include <iostream>
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include <glimac/SDLWindowManager.hpp>
#include <glimac/Camera.hpp>
#include <GL/glew.h>
#include "./../../Engine.hpp"
#include "GraphicalUserInterface.hpp"
#include "SceneManager.hpp"
#include <glimac/TrackBallCamera.hpp>
#include "FpsManager.hpp"

/*
 *  Main class of the graphic section.
 *  Is in charge of rendering the 3D scene and the 2D GUI.
 *  Its role is to divide this large task to several other components that will perform smaller tasks.
 *
 *  The _state attribute helps to know what to draw. It is a string composed of three information :
 *  Possible states : {SCENE / GUI / CAMERA}
 *  SCENE --> SCENE
 *  GUI --> MENU_GUI / IN_GAME_GUI / IN_GAME_PAUSE_GUI
 *  CAMERA --> MENU_CAMERA / IN_GAME_CAMERA_LOCKED / IN_GAME_CAMERA_UNLOCKED
 *
 *  Then, we just have to build the state using these three components.
 *  Example : "SCENE IN_GAME_CAMERA MENU_GUI"
 *  This state will render the SCENE using the IN_GAME_CAMERA and will draw the MENU_GUI on top of it
 */

class GraphicsEngine: public Engine{
private:
    uint32_t _screen_width;
    uint32_t _screen_height;
    FpsManager _fps_manager;
    glimac::Camera _camera;
    glimac::TrackBallCamera _menu_camera;
    GraphicalUserInterface _gui;
    SceneManager _scene;
    std::string _state;
public:
    GraphicsEngine(const uint32_t& sw, const uint32_t sh);
    ~GraphicsEngine() = default;

    void initialize(CookingPot& cp, const unsigned int& frame_rate);
    void loadResources() override;
    void draw(glimac::SDLWindowManager& window_manager);
    void handleEvents(EventsManager& events_manager) override;
    void freeResources() override;

    inline void setState(const std::string& new_state){_state = new_state;};
    inline std::string getState() const noexcept{return _state;};

    inline glimac::Camera& getCamera() noexcept{return _camera;};
    inline GraphicalUserInterface& getGUI(){return _gui;};
    inline SceneManager& getScene(){return _scene;};
};
