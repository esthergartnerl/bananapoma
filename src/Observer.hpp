#pragma once

/*
 * Code from : https://riptutorial.com/cplusplus/example/24695/observer-pattern
 */

class Observer{
public:
    virtual ~Observer() = default;
    virtual void update() = 0;
};