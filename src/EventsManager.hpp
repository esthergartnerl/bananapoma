#pragma once
#include <SDL/SDL.h>
#include <glimac/SDLWindowManager.hpp>
class GameEngine;
class GraphicsEngine;
class SoundEngine;

/*
 * This class manages the events.
 * Its role is to divide this large task to several other components that will perform smaller tasks.
 */

class EventsManager{
private:
    SDL_Event _event;
    bool* _running;
    GraphicsEngine* _gra_en;
    GameEngine* _gam_en;
    SoundEngine* _sou_en;
    glimac::SDLWindowManager* _w_m;
public:
    EventsManager():_event(), _running(nullptr), _gra_en(nullptr), _gam_en(nullptr), _sou_en(nullptr), _w_m(nullptr){};
    ~EventsManager() = default;

    void initialize(bool& running, GraphicsEngine& gra_en, GameEngine& gam_en, SoundEngine& sou_en,
                    glimac::SDLWindowManager& w_m);
    void manageEvents();

    inline SDL_Event& event(){return _event;};
    inline GraphicsEngine* graphicsEngine(){return _gra_en;};
    inline SoundEngine* soundEngine(){return _sou_en;};
};