#pragma once
#include <glimac/SDLWindowManager.hpp>
#include "GraphicsEngine/include/GraphicsEngine.hpp"
#include "GameEngine/include/GameEngine.hpp"
#include "SoundEngine/include/SoundEngine.hpp"
#include "EventsManager.hpp"

/*
 * Main class. Contains everything.
 * Call every methods one after the other (in main.cpp) to make it work.
 */

class Game{
private:
    static uint32_t _screen_width;
    static uint32_t _screen_height;
    static unsigned int _frame_rate;
    static Game _singleton;
    Game() noexcept;

    bool _running;
    glimac::SDLWindowManager _window_manager;
    EventsManager _events_manager;
    GraphicsEngine _graphics_engine;
    GameEngine _game_engine;
    SoundEngine _sound_engine;
public:
    inline static Game& get() noexcept{return _singleton;};

    void initialize();
    void load();
    void run();
    void free();
};
