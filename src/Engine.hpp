#pragma once

class EventsManager;

/*
 * This class defines what should an Engine be able to do
 * Then, the functions are overwritten to adapt to the different types of Engines
 */

class Engine{
public:
    Engine() = default;
    ~Engine() = default;

    virtual void loadResources() = 0;
    virtual void freeResources() = 0;
    virtual void handleEvents(EventsManager& events_manager) = 0;
};
