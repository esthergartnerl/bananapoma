#include "EventsManager.hpp"
#include "GraphicsEngine/include/GraphicsEngine.hpp"
#include "GameEngine/include/GameEngine.hpp"
#include "SoundEngine/include/SoundEngine.hpp"

void EventsManager::initialize(bool& running, GraphicsEngine& gra_en, GameEngine& gam_en, SoundEngine& sou_en,
                               glimac::SDLWindowManager& w_m) {
    _running = &running;
    _gra_en = &gra_en;
    _gam_en = &gam_en;
    _sou_en = &sou_en;
    _w_m = &w_m;
}

void EventsManager::manageEvents() {
    SDLKey key_pressed_code;
    auto& main_menu_buttons = _gra_en->getGUI().getMainMenuButtons();
    std::string gra_state = _gra_en->getState();
    while (_w_m->pollEvent(_event)){
        _sou_en->handleEvents(*this);
        _gam_en->handleEvents(*this);
        _gra_en->handleEvents(*this);
        key_pressed_code = _event.key.keysym.sym;
        switch (_event.type) {
            case SDL_QUIT:
                *_running = false;
                break;
            case SDL_MOUSEBUTTONDOWN:
                if(_event.button.button == SDL_BUTTON_LEFT)
                    if(main_menu_buttons["quit"].first.isHovered())
                        *_running = false;
                break;
            case SDL_KEYDOWN:
                // Triggers the pause menu ("ESCAPE" in game)
                if(gra_state.find("IN_GAME_GUI") != std::string::npos)
                    if(key_pressed_code == SDLK_ESCAPE && _gra_en->getState().find("IN_GAME_GUI"))
                        _gra_en->setState("SCENE IN_GAME_PAUSE_GUI IN_GAME_CAMERA_LOCKED");
                // Camera movement events
                if(gra_state.find("IN_GAME_CAMERA_UNLOCKED") != std::string::npos){
                    if(key_pressed_code == SDLK_LSHIFT) _gra_en->getCamera().setRun(2.0);
                    if(key_pressed_code == SDLK_z) _gra_en->getCamera().setMoveForward(1);
                    if(key_pressed_code == SDLK_s) _gra_en->getCamera().setMoveForward(-1);
                    if(key_pressed_code == SDLK_q) _gra_en->getCamera().setMoveLeft(1);
                    if(key_pressed_code == SDLK_d) _gra_en->getCamera().setMoveLeft(-1);
                }
                break;
            case SDL_KEYUP:
                // Resets camera movements
                if(gra_state.find("IN_GAME_CAMERA_UNLOCKED") != std::string::npos){
                    if(key_pressed_code == SDLK_LSHIFT) _gra_en->getCamera().setRun(0);
                    if(key_pressed_code == SDLK_z) _gra_en->getCamera().setMoveForward(0);
                    if(key_pressed_code == SDLK_s) _gra_en->getCamera().setMoveForward(0);
                    if(key_pressed_code == SDLK_q) _gra_en->getCamera().setMoveLeft(0);
                    if(key_pressed_code == SDLK_d) _gra_en->getCamera().setMoveLeft(0);
                }
        }
    }
}

