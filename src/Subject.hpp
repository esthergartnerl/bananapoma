#pragma once
#include "Observer.hpp"
#include <vector>

/*
 * Code from : https://riptutorial.com/cplusplus/example/24695/observer-pattern
 */

class Subject{
private:
    std::vector<Observer*> _observers;
public:
    Subject():_observers(){};
    ~Subject() = default;

    inline void registerObserver(Observer& new_obs){_observers.push_back(&new_obs);};
    inline void unregisterObserver(Observer& obs){std::remove(_observers.begin(), _observers.end(), &obs);};
    inline void notifyObservers(){for(auto* obs: _observers) obs->update();};
};