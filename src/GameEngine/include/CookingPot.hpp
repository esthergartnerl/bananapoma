#pragma once
#include "Ingredient.hpp"
#include "RecipesManager.hpp"
#include "../../Subject.hpp"
#include <algorithm>

/*
 * Manages the interactions with the cooking pot (add ingredient, cook ...)
 */

class CookingPot: public Subject{
private:
    std::vector<Ingredient> _ingredients;
    unsigned int _graphical_id;
    Recipe* _last_recipe;
public:
    CookingPot():_ingredients(), _graphical_id(), _last_recipe(nullptr){};
    ~CookingPot() = default;

    void addIngredient(const Ingredient& new_ingredient);
    bool cook(RecipesManager& known_recipes);
    void clear();

    inline void setGraphicalId(const unsigned int& id){_graphical_id = id;};

    inline std::vector<Ingredient> getIngredients() const noexcept{return _ingredients;};
    inline unsigned int getGraphicalId() const noexcept{return _graphical_id;};
    inline const Recipe* getLastRecipe() const noexcept{return _last_recipe;};
};