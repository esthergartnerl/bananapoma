#pragma once
#include <string>
#include <GL/glew.h>

/*
 * Represents an ingredient
 * _graphical_id = to identify the ingredient on the screen (using the stencil buffer)
 * _name = name of the ingredient
 */

class Ingredient{
private:
    GLuint _graphical_id;
    std::string _name;
public:
    Ingredient(const GLuint& gr_id, const std::string& type): _graphical_id(gr_id), _name(type){};
    ~Ingredient() = default;

    inline GLuint getGraphicalId() const noexcept{return _graphical_id;};
    inline std::string getName() const noexcept{return _name;};

    inline bool operator==(const Ingredient& ingredient) const{return ingredient._name == _name;};
};