#pragma once
#include <vector>
#include <iostream>
#include <string>
#include <algorithm>

/*
 * Represents a recipe
 * _name = name of the recipe
 * _ingredient_names = name of all the ingredients to do the recipe
 */

class Recipe{
private:
    std::string _name;
    std::vector<std::string> _ingredient_names;
public:
    Recipe(const std::vector<std::string>& ingredients, const std::string& name = ""):_name(name),
    _ingredient_names(ingredients){};
    ~Recipe() = default;

    bool compare(const Recipe& compared_recipe) const;

    inline std::vector<std::string> getIngredients() const noexcept{return _ingredient_names;};
    inline std::string getName() const noexcept{return _name;};
};