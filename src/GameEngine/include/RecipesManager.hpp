#pragma once
#include "Recipe.hpp"
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <glimac/FilePath.hpp>

/*
 * Manages the interactions with all the available recipes
 * _recipes = all the available recipes
 */

class RecipesManager{
private:
    std::vector<Recipe> _recipes;
    glimac::FilePath _file_path;
public:
    RecipesManager() = default;
    ~RecipesManager() = default;

    inline void initialize(const std::string& path){_file_path = path;};
    void loadRecipes();
    bool compareAll(const Recipe& compared_recipe) const;

    Recipe* getRecipeByIngredients(const std::vector<std::string>& ings);
};