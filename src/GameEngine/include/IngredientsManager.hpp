#pragma once
#include "Ingredient.hpp"
#include <glimac/FilePath.hpp>
#include <vector>
#include <fstream>
#include <iostream>
#include <boost/algorithm/string.hpp>
#include "CookingPot.hpp"

/*
 * This class is used to manage all the ingredients and to "save" the ingredient that
 * the player is currently holding (or not)
 */

class IngredientsManager{
private:
    std::vector<Ingredient> _ingredients;
    Ingredient* _selected_ing;
public:
    IngredientsManager():_ingredients(), _selected_ing(nullptr){};
    ~IngredientsManager() = default;

    void readIngsFromModelsFile(const glimac::FilePath& scene_path, CookingPot& cp);
    inline void releaseSelectedIngredient(){_selected_ing = nullptr;};

    void setSelectedIngredient(const unsigned int& id_selected_obj);

    inline Ingredient* getSelectedIngredient(){return _selected_ing;};

};