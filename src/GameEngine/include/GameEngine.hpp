#pragma once
#include "./../../Engine.hpp"
#include "CookingPot.hpp"
#include "RecipesManager.hpp"
#include "IngredientsManager.hpp"

/*
 * Main class of the game section (the one that's in charge of the "logic")
 * Its role is to divide this large task to several other components that will perform smaller tasks.
 */

class GameEngine: public Engine{
private:
    RecipesManager _recipes_manager;
    CookingPot _cooking_pot;
    IngredientsManager _ingredients_manager;
public:
    GameEngine() = default;
    ~GameEngine() = default;

    void initialize();
    void loadResources() override;
    void handleEvents(EventsManager& events_manager) override;
    void freeResources() override;

    inline IngredientsManager& getIngredientManager(){return _ingredients_manager;};
    inline CookingPot& getCookingPot() noexcept{return _cooking_pot;};
};
