#include "./../include/GameEngine.hpp"
// These includes are necessary because of the mutual inclusion between EventsManager and Engine
#include "../../EventsManager.hpp"
#include "../../GraphicsEngine/include/GraphicsEngine.hpp"
#include "../../SoundEngine/include/SoundEngine.hpp"

void GameEngine::initialize() {
#ifdef VERBOSE
    std::cout << "Initialisation du moteur de jeu" << std::endl;
#endif
    _recipes_manager.initialize("assets/recipes.txt");
    _ingredients_manager.readIngsFromModelsFile("assets/scene.txt", _cooking_pot);
}

void GameEngine::loadResources() {
#ifdef VERBOSE
    std::cout << "Chargement des ressources du moteur de jeu" << std::endl;
#endif
    _recipes_manager.loadRecipes();
}

void GameEngine::handleEvents(EventsManager& events_manager) {
    std::string gra_state = events_manager.graphicsEngine()->getState();
    // GLuint id_selected = events_manager.graphicsEngine()->getScene().getSelectedObject(); // Using stencil buffer
    GLuint id_selected = events_manager.graphicsEngine()->getScene().getSelectedObject(events_manager.graphicsEngine()->getCamera().getViewMatrix()); // Using c3ga
    Ingredient* selected_ingredient = _ingredients_manager.getSelectedIngredient();

    if(events_manager.event().type == SDL_MOUSEBUTTONDOWN){
        switch (events_manager.event().button.button) {
            case SDL_BUTTON_RIGHT:
                if(gra_state.find("IN_GAME_CAMERA_UNLOCKED") != std::string::npos){ // Player is in game
                    if(this->_ingredients_manager.getSelectedIngredient() != nullptr){ // Player holds an ingredient
                        this->getIngredientManager().releaseSelectedIngredient(); // Release the ingredient
                        events_manager.graphicsEngine()->getGUI().setSelectedIng("");
                        events_manager.soundEngine()->playSound("OBJ_DROP");
                    }
                }
                break;
            case SDL_BUTTON_LEFT:
                // Ingredients selection using the mouse
                if(gra_state.find("IN_GAME_CAMERA_UNLOCKED") != std::string::npos){ // Checks if "in game"
                    if(selected_ingredient == nullptr) { // Player does not have anything in his hands
                        if(id_selected == _cooking_pot.getGraphicalId() && !_cooking_pot.getIngredients().empty()){ // Cooking pot selected
                            if(_cooking_pot.cook(_recipes_manager)) // Time to cook !
                                events_manager.soundEngine()->playSound("RECIPE_SUCCESS");
                            else
                                events_manager.soundEngine()->playSound("RECIPE_FAIL");
                        }
                        else if(id_selected > 0 && id_selected != _cooking_pot.getGraphicalId()){ // Grabs an ingredient
                            _ingredients_manager.setSelectedIngredient(id_selected);
                            std::string text_selected_ing = "Dans ta main : " +
                                    _ingredients_manager.getSelectedIngredient()->getName();
                            events_manager.graphicsEngine()->getGUI().setSelectedIng(text_selected_ing);
                            events_manager.soundEngine()->playSound("OBJ_GET");
                        }
                    }
                    if(id_selected == _cooking_pot.getGraphicalId()){ // Cooking pot selected
                        if(selected_ingredient != nullptr){ // Adds the held ingredient into the pot
                            _cooking_pot.addIngredient(*selected_ingredient);
                            _ingredients_manager.releaseSelectedIngredient();
                            events_manager.graphicsEngine()->getGUI().setSelectedIng("");
                            events_manager.soundEngine()->playSound("OBJ_DROP");
                        }
                    }
                }
                break;
            default:
                break;
        }
    }
}

void GameEngine::freeResources() {
#ifdef VERBOSE
    std::cout << "Libération des ressources du moteur de jeu" << std::endl;
#endif
    // Unused here. But was designed to be used ...
}