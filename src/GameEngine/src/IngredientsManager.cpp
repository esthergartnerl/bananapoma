#include "../include/IngredientsManager.hpp"

void IngredientsManager::setSelectedIngredient(const unsigned int& id_selected_obj) {
    // Checks if the given ingredient id exists in the ingredients list
    for(auto& ing: _ingredients)
        if(ing.getGraphicalId() == id_selected_obj)
            // Sets the selected ingredient
            _selected_ing = &ing;
}

void IngredientsManager::readIngsFromModelsFile(const glimac::FilePath& scene_path, CookingPot& cp) {
    // Reads all the ingredients written in the 3d models file
    std::ifstream file;
    file.open(scene_path);
    if (!file.is_open()) {
        std::cerr << "ERROR::SceneManager : impossible d'ouvrir " << scene_path << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string line;
    std::vector<std::string> s_l; // after the split : <data1, data2, ..., dataN>
    unsigned int id = 0; // graphical id
    while (getline(file, line)) { // Read all the lines
        boost::split(s_l, line, [](char c) { return c == ';'; });
        if (s_l.at(0).find("dynamic") != std::string::npos) { // Check if it's a "dynamic" model
            if (s_l.at(0).find("marmite") == std::string::npos) { // Check if it's the cooking pot (because it's not an ingredient)
                boost::split(s_l, s_l.at(0), [](char c) { return c == '_'; });
                _ingredients.emplace_back(++id, s_l.at(1)); // adds the ingredient to the list
            } else {
                cp.setGraphicalId(++id);
            }
        }
    }
    file.close();
#ifdef VERBOSE
    std::cout << "Ingrédients chargés : " << std::endl;
    for (auto &ing: _ingredients)
        std::cout << "ID = " << ing.getGraphicalId() << ", nom = " << ing.getName() << std::endl;
#endif
}