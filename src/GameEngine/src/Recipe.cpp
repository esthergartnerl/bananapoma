#include "../include/Recipe.hpp"

bool Recipe::compare(const Recipe &compared_recipe) const{
    // Checks if the recipes do not have the same amount of ingredients
    if(compared_recipe._ingredient_names.size() != _ingredient_names.size()){
        return false;
    }
    std::string comp_ing; // compared ingredient
    for (size_t i = 0; i < _ingredient_names.size(); ++i) { // For each ingredient in the given recipe (compared_recipe)
        comp_ing = compared_recipe.getIngredients().at(i);
        if(!std::any_of(_ingredient_names.begin(), _ingredient_names.end(),
                        [comp_ing](const std::string& ingredient){return ingredient == comp_ing;}))
            // If one, at least, does not match any of the current recipe, then it does not match at all !
            return false;
    }
    return true;
}
