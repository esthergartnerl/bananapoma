#include "../include/RecipesManager.hpp"

void RecipesManager::loadRecipes() {
    std::ifstream file;
    file.open("assets/recipes.txt");
    if(!file.is_open()){
        std::cerr << "ERROR::RecipesManager : impossible d'ouvrir " << _file_path << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string line;
    std::vector<std::string> s_l;
    while (getline(file, line))
    {
        std::string name;
        boost::split(s_l, line, [](char c){return c == ';';});
        name = s_l.at(0);
        boost::split(s_l, s_l.at(1), [](char c){return c == '+';});
        _recipes.emplace_back(s_l, name);
    }
    file.close();
#ifdef VERBOSE
    std::cout << "Recettes chargées :" << std::endl;
    for(auto& e: _recipes){
        std::cout << "Nom = " << e.getName() << ", ingrédients = ";
        for(auto& ing: e.getIngredients())
            std::cout << ing << ", ";
        std::cout << "\n";
    }
#endif
}

bool RecipesManager::compareAll(const Recipe &compared_recipe) const {
    // Compare all the known recipes to the given recipe
    return std::any_of(_recipes.begin(), _recipes.end(),
                       [compared_recipe](const Recipe& recipe){return recipe.compare(compared_recipe);});
}

Recipe* RecipesManager::getRecipeByIngredients(const std::vector<std::string> &ings){
    // Returns the recipe that corresponds to the given list of ingredients
    Recipe player_recipe = Recipe(ings);
    for(auto& recipe: _recipes)
        if(recipe.compare(player_recipe))
            return &recipe;
    return nullptr;
}