#include "../include/CookingPot.hpp"

void CookingPot::addIngredient(const Ingredient& new_ing) {
    // Checks if the given ingredient is not already in the pot
    if(!std::any_of(_ingredients.begin(), _ingredients.end(),[new_ing](const Ingredient& ing){return ing == new_ing;}))
    {
        // Adds it to the pot
        _ingredients.push_back(new_ing);
        this->notifyObservers();
    }
}

bool CookingPot::cook(RecipesManager& known_recipes) {
    bool success = false;
    // Checks if there is anything to cook
    if(!_ingredients.empty()){
        // Converts the ingredient list into a list containing ingredient types
        std::vector<std::string> temp_ingredients_list;
        for(auto& ing: _ingredients){temp_ingredients_list.push_back(ing.getName());}
        Recipe player_recipe(temp_ingredients_list);
        if(known_recipes.compareAll(player_recipe)){
            // If the recipe exists, it's a success !
            _last_recipe = known_recipes.getRecipeByIngredients(temp_ingredients_list);
            success = true;
        }
        else{
            _last_recipe = nullptr;
            success = false;
        }
        this->clear();
        return success;
    }
    return success;
}

void CookingPot::clear() {
    // Clears the pot
    _ingredients.clear();
    this->notifyObservers();
}