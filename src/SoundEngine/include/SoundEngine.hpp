#pragma once
#include <iostream>
#include "./../../Engine.hpp"
#include <SDL/SDL_mixer.h>
#include <unordered_map>
#include <glimac/FilePath.hpp>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <vector>

class SoundEngine: public Engine{
private:
    Mix_Music *_music;
    std::unordered_map<std::string, std::pair<Mix_Chunk*, int>> _sounds;

    static std::unordered_map<std::string, std::string> readSoundsFromFile(const glimac::FilePath& sounds_path);
public:
    SoundEngine() = default;
    ~SoundEngine() = default; // no need to Mix_Quit(), SDL_Quit() already does it

    void initialize();
    void loadResources() override;
    void handleEvents(EventsManager& events_manager) override;
    void freeResources() override;

    inline void playMusic() const {Mix_PlayMusic(_music, -1.);};
    inline void playSound(const std::string& sound_name)
    {Mix_PlayChannel(_sounds[sound_name].second, _sounds[sound_name].first, 0);};
};
