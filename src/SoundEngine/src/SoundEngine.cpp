#include "./../include/SoundEngine.hpp"
// These includes are necessary because of the mutual inclusion between EventsManager and Engine
#include "../../EventsManager.hpp"
#include "../../GraphicsEngine/include/GraphicsEngine.hpp"

std::unordered_map<std::string, std::string> SoundEngine::readSoundsFromFile(const glimac::FilePath& sounds_path) {
    std::ifstream file;
    file.open(sounds_path);
    if(!file.is_open()){
        std::cerr << "ERROR::SceneManager : impossible d'ouvrir " << sounds_path << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string line;
    std::vector<std::string> s_l;
    std::unordered_map<std::string, std::string> sounds_infos;
    while (getline(file, line))
    {
        boost::split(s_l, line, [](char c){return c == ';';});
        sounds_infos[s_l.at(0)] = s_l.at(1);
    }
    file.close();
    return sounds_infos;
}

void SoundEngine::initialize() {
#ifdef VERBOSE
    std::cout << "Initialisation du moteur audio" << std::endl;
#endif
    if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0){
        std::cerr << Mix_GetError() << std::endl;
        exit(EXIT_FAILURE);
    }
    // Music volume
    Mix_VolumeMusic(MIX_MAX_VOLUME / 10);
}

void SoundEngine::loadResources() {
#ifdef VERBOSE
    std::cout << "Chargement des ressources du moteur audio" << std::endl;
#endif
    // Loads the music
    _music = Mix_LoadMUS("assets/musics/sound_track_banana_pomma.mp3");

    // Channels allocation for sounds
    auto sounds_infos = readSoundsFromFile("assets/sounds.txt");
    Mix_AllocateChannels(sounds_infos.size());
    for(size_t i = 0; i < sounds_infos.size(); ++i)
        Mix_Volume(i, MIX_MAX_VOLUME/2);
    // Loads the sounds
    int channel = -1;
    for(auto& sound_info: sounds_infos)
        _sounds[sound_info.first] = {Mix_LoadWAV(sound_info.second.c_str()), ++channel};
}

void SoundEngine::handleEvents(EventsManager &events_manager) {
    std::string gra_en_state = events_manager.graphicsEngine()->getState();
    if(events_manager.event().key.keysym.sym == SDLK_ESCAPE && events_manager.event().type == SDL_KEYDOWN)
        if(gra_en_state.find("IN_GAME_CAMERA_UNLOCKED") != std::string::npos)
            Mix_PauseMusic();
    if(events_manager.event().button.button == SDL_BUTTON_LEFT && events_manager.event().type == SDL_MOUSEBUTTONDOWN){
        if(gra_en_state.find("IN_GAME_PAUSE_GUI") != std::string::npos)
            for(auto& button: events_manager.graphicsEngine()->getGUI().getPauseMenuButtons())
                if(button.second.first.isHovered())
                    Mix_ResumeMusic();
    }
}

void SoundEngine::freeResources() {
#ifdef VERBOSE
    std::cout << "Libération des ressources du moteur audio" << std::endl;
#endif
    Mix_FreeMusic(_music);
    for(auto& sound: _sounds)
        Mix_FreeChunk(sound.second.first);
    Mix_CloseAudio();
}