#pragma once
#include <GL/glew.h>
#include "glm.hpp"
#include <iostream>

namespace glimac{

    class Camera
    {
    private:
        glm::vec3 m_Position;
        float m_fPhi;
        float m_fTheta;
        float m_Speed;

        glm::vec3 m_FrontVector;
        glm::vec3 m_LeftVector;
        glm::vec3 m_UpVector;

        int m_Movef; // move forward/none/backward (1, 0, -1)
        int m_Movel; // move left/none/right (1, 0, -1)
        float m_Run; // accelerates the movement

        void computeDirectionVectors();
        void moveFront(const float& t);
        void moveLeft(const float& t);
    public:
        Camera(): m_Position(glm::vec3(0.f, 0.75f, -2.f)), m_fPhi(-M_PI/1.5f), m_fTheta(0), m_Speed(1.0f),
        m_Movef(0), m_Movel(0), m_Run(0){this->computeDirectionVectors();};
        ~Camera() = default;

        inline void setPosition(const glm::vec3& new_pos){m_Position = new_pos;};
        inline void setMoveForward(const int& status){ m_Movef = status;};
        inline void setMoveLeft(const int& status){ m_Movel = status;};
        inline void setRun(const float& run){ m_Run = run;};

        inline void rotateLeft(const float &angle){m_fPhi += glm::radians(angle);};
        void rotateFront(const float &angle);

        void updatePosition(const float& fps_correction);
        glm::mat4 getViewMatrix();

        void setSpeed(const float& new_speed){ m_Speed = new_speed;};

        const glm::vec3& getPosition() const { return m_Position; }
    };

}
