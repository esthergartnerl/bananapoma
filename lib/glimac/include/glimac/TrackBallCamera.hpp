#pragma once
#include <GL/glew.h>
#include "glm.hpp"

namespace glimac{

  class TrackBallCamera
  {
  private:
    float m_fDistance;
    float m_fAngleX;
    float m_fAngleY;
    glm::vec3 m_Position;

//    inline void rotateLeft(const float &aY){m_fAngleY += aY;};
//    inline void rotateUp(const float &aX){m_fAngleX += aX;};
  public:
    explicit TrackBallCamera(const float &d = 5.f, const glm::vec2& angles = {0.0f, 0.0f}):m_fDistance(d),
    m_fAngleX(angles.x), m_fAngleY(angles.y), m_Position(0.f){};
    ~TrackBallCamera()= default;

//    inline void moveFront(const float &delta){m_fDistance -= delta;};
    void rotateAroundPoint(const glm::vec2& angles,  const float& fps_correction);
    void setPosition(const glm::vec3& new_pos){m_Position = new_pos;};

    [[nodiscard]] glm::mat4 getViewMatrix() const;
  };
}
