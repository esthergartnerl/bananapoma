#pragma once
#include <cstdint>
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#include "glm.hpp"

namespace glimac {

    class SDLWindowManager {
    private:
        uint32_t m_width;
        uint32_t m_height;
        const char* m_title;
    public:
        SDLWindowManager(const uint32_t width, const uint32_t height, const char* title)
        :m_width(width), m_height(height), m_title(title){};
        ~SDLWindowManager()= default;

        void initializeSDLOpenGlContext();
        void swapBuffers();
        bool pollEvent(SDL_Event& e);
        [[nodiscard]] inline uint32_t getTicks() const noexcept{return SDL_GetTicks();};
        inline void freeResources(){SDL_Quit();};
    };

}
