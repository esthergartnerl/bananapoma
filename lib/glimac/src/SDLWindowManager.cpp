#include "glimac/SDLWindowManager.hpp"
#include <iostream>

namespace glimac {

    void SDLWindowManager::initializeSDLOpenGlContext() {
        if(0 != SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO)) {
            std::cerr << SDL_GetError() << std::endl;
            exit(EXIT_FAILURE);
        }
        // Initializes the stencil buffer : 8 bits per pixel (values : 0 to 255)
        SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
        if(!SDL_SetVideoMode(m_width, m_height, 32, SDL_OPENGL)) {
            std::cerr << SDL_GetError() << std::endl;
            exit(EXIT_FAILURE);
        }
        SDL_WM_SetCaption(m_title, nullptr);
    }

    bool SDLWindowManager::pollEvent(SDL_Event& e) {
        return SDL_PollEvent(&e);
    }

    void SDLWindowManager::swapBuffers() {
        SDL_GL_SwapBuffers();
    }

}
