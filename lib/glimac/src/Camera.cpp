#include <glimac/Camera.hpp>

namespace glimac{

    void Camera::moveFront(const float &t)
    {
        // does not update the Y coordinate because we don't want the player to fly
        m_Position.x += t * m_FrontVector.x;
        m_Position.z += t * m_FrontVector.z;
    }

    void Camera::moveLeft(const float &t)
    {
        // does not update the Y coordinate because we don't want the player to fly
        m_Position.x += t * m_LeftVector.x;
        m_Position.z += t * m_LeftVector.z;
    }

    void Camera::rotateFront(const float &angle)
    {
        // Limits the angle of m_fTheta to [-90deg; 90deg]
        float new_angle = m_fTheta + glm::radians(angle);
        if(new_angle >= -M_PI/2.0f && new_angle <= M_PI/2.0f)
            m_fTheta = new_angle;
    }

    void Camera::computeDirectionVectors()
    {
        float sin_theta = glm::sin(m_fTheta);
        float cos_theta = glm::cos(m_fTheta);
        float sin_phi = glm::sin(m_fPhi);
        float cos_phi = glm::cos(m_fPhi);

        m_FrontVector = glm::vec3(cos_theta*sin_phi, sin_theta, cos_theta*cos_phi);
        // I use : sin(x + pi/2) = cos(x) et cos(x + pi/2) = -sin(x)
        m_LeftVector = glm::vec3(cos_phi, 0.f, -sin_phi);
        m_UpVector = glm::cross(m_FrontVector, m_LeftVector);
    }

    glm::mat4 Camera::getViewMatrix()
    {
        this->computeDirectionVectors();
        return glm::lookAt(m_Position, m_Position + m_FrontVector, m_UpVector);
    }

    void Camera::updatePosition(const float& fps_correction)
    {
        /* fps_correction corresponds to the time between two frames. We use this factor to make sure that every
         * player can move at the same speed, independently from the performances of their computers.
         */
        // Updates the camera position
        float speed = m_Run + m_Speed;
        if(m_Movef == 1) moveFront(fps_correction * speed);
        if(m_Movef == -1) moveFront(-fps_correction * speed);
        if(m_Movel == 1) moveLeft(fps_correction * speed);
        if(m_Movel == -1) moveLeft(-fps_correction * speed);
    }

}
