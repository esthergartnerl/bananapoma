#include <glimac/TrackBallCamera.hpp>

namespace glimac{

    glm::mat4 TrackBallCamera::getViewMatrix() const
    {
        glm::mat4 MVMatrix = glm::translate(glm::mat4(1.f),glm::vec3(0.f, 0.f, -m_fDistance));
        MVMatrix = glm::rotate(MVMatrix, glm::radians(m_fAngleX),glm::vec3(1.f, 0.f, 0.f));
        MVMatrix = glm::rotate(MVMatrix, glm::radians(m_fAngleY),glm::vec3(0.f, 1.f, 0.f));
        MVMatrix = glm::translate(MVMatrix,m_Position);
        return MVMatrix;
    }

    void TrackBallCamera::rotateAroundPoint(const glm::vec2& angles,  const float& fps_correction) {
        // Resets the angles to 0 when they reach 2*M_PI (complete revolution)
        if(glm::radians(m_fAngleX) >= 2*M_PI) m_fAngleX = 0.0f;
        if(glm::radians(m_fAngleY) >= 2*M_PI) m_fAngleY = 0.0f;
        m_fAngleY += angles.y;
        m_fAngleX += angles.x;
    }

}
